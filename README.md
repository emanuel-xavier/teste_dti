# teste_dti

# Premissas assumidas
- A formatação da entrada pode ser passada errada na hora de executar o sistema.
- Os petshops podem ou nao manter o mesmo valor durante dias de semana e finais de semana.
- Todos os petshops podem ter o mesmo valor final e variar apeanas na distância.

# Decisões de projeto
- Todos os petshops tem são iguais só diferem na função para decidir o preço nos fim de semana, entao foi feito uma herança de classes para reaproveitamento de código.
- Pode ser que todos os petshops cobrem o mesmo preço em uma determinada data para um determinado número de animais grandes e pequenos, por isso foi feito a sobrecarga dos operadores ">", "=" e "<" e foi usado um algorítmo de ordenção para evitar grandes quantidades de if e elses que cresceriam junto com o número de petshops adicionados.
- O sistema foi separado em pastas, na pasta **models** é onde fica a classe de petshop, na pasta **utils** é onde ficam funções que serão usadas pelo código mas não fazem parte das classes.
- Caso a formatação da entrada esteja errada pode háver problemas, se passar a data no formato 9/9/09 o temos erro no identificador de fins de semana, para isso é feita uma validação das entradas.

## Como compilar
**OBS: Usando linux pode ser necessário trocar contrabarra por barra**
###### sistema
    - 1: No terminal certifique-se que está na pasta src.
    - 2: com um compilador para c++ use o comando "g++ main.cpp models\petshop.cpp utils\utils.cpp -Wall -o main.exe".
###### testes
    - 1: No terminal certifique-se que está na pasta src.
    - 2: com um compilador para c++ use o comando "g++ tests\testesMain.cpp models\petshop.cpp utils\utils.cpp tests\unit\petshopTest.cpp tests\unit\utilsTest.cpp -Wall -o teste.exe".

## Como executar
**OBS: Usando linux pode ser necessário trocar contrabarra por barra**
###### sistema
    - 1: No terminal certifique-se que está na pasta src.
    - 2: com um compilador para c++ use o comando ".\main.exe data número_de_cachorros_pequenos número_de_cachorros_grandes".

certifique-se que a data está no formato **dd/mm/aaaa** e que os números de animais são mesmo números
###### testes
    - 1: No terminal certifique-se que está na pasta src.
    - 2: com um compilador para c++ use o comando ".\teste.exe".