#include "../../models/petshop.hpp"
#include "petshopTest.hpp"

class petTest : public PetShop {
    public:
        petTest(string nome, float pequeno, float grande, float distancia) : PetShop(nome, pequeno, grande, distancia) {
        }

        ~petTest () {

        }

        void fimDeSemana (float &pequeno, float &grande) {
            pequeno = this->pequeno * 2;
            grande = this->grande * 2;
        }
};

void fimDeSemana () {
    PetShop *petshop;
    petshop = new petTest("teste", 20, 45, 2);

    float p = 0, g = 0;

    petshop->fimDeSemana(p, g);

    assert(p == 40);
    assert(g == 90);

    petshop->fimDeSemana(p, g);

    assert(p == 40);
    assert(g == 90);

}

void calcularPreco() {
    PetShop *petshop;
    petshop = new petTest("teste", 1, 1, 2);

    float preco;
    
    preco = petshop->calcularPreco(false, 1, 1);

    assert(preco == 2);

    preco = petshop->calcularPreco(true, 1, 1);

    assert(preco == 4);

    preco = petshop->calcularPreco(false, 4, 2);

    assert(preco == 6);

    preco = petshop->calcularPreco(true, 4, 2);

    assert(preco == 12);

}