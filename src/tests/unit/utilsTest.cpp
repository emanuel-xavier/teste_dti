#include "utilsTest.hpp"

void operatorMaior () {
    Util u1 = Util(100, 2, 0), u2 = Util(150, 1.7, 1), u3 = Util(150, 1.3, 2);

    assert(u2 > u1);
    assert(u3 > u1);
    assert(u2 > u3);
}

void operatorMenor () {
    Util u1 = Util(100, 2, 0), u2 = Util(150, 1.7, 1), u3 = Util(150, 1.3, 2);

    assert(u2 < u1);
    assert(u3 < u1);
    assert(u2 < u3);
}

void operatorIgual () {
    Util u1 = Util(100, 2, 0), u2;

    u2 = u1;
    
    assert(u2.preco == 100);
    assert(u2.distancia == 2);
    assert(u2.posicao == 0);
}
#include <iostream>
void EhFimDeSemana () {
    assert(!EhFinalDeSemana("14/07/2020"));
    assert(EhFinalDeSemana("08/08/2020"));
    assert(!EhFinalDeSemana("08/01/2020"));
    assert(!EhFinalDeSemana("06/01/2021"));
    assert(EhFinalDeSemana("11/03/2018"));
}

void quickSort () {
    Util u1 = Util(100, 2, 0), u2 = Util(150, 1.7, 1), u3 = Util(150, 1.3, 2);
    // u2 > u3 > u1

    Util vet[3];
    vet[0] = u1;
    vet[1] = u2;
    vet[2] = u3;

    quickSort(vet, 0, 2);
    assert(vet[0].posicao == 0);
    assert(vet[1].posicao == 2);
    assert(vet[2].posicao == 1);
}

class MeuCaninoFeliz : public PetShop {
    public:
        MeuCaninoFeliz(string nome, float pequeno, float grande, float distancia) : PetShop(nome, pequeno, grande, distancia) {
        }

        ~MeuCaninoFeliz () {

        }

        void fimDeSemana (float &pequeno, float &grande) {
            pequeno = this->pequeno * 1.2;
            grande = this->grande * 1.2;
        }
};

class VaiRex : public PetShop {
    public:
        VaiRex(string nome, float pequeno, float grande, float distancia) : PetShop(nome, pequeno, grande, distancia) {
        }

        ~VaiRex () {

        }

        void fimDeSemana (float &pequeno, float &grande) {
            pequeno = this->pequeno + 5;
            grande = this->grande + 5;
        }
};

class ChowChawgas : public PetShop {
    public:
        ~ChowChawgas() {

        }

        ChowChawgas(string nome, float pequeno, float grande, float distancia) : PetShop(nome, pequeno, grande, distancia) {
        }
};

void decidirMelhorOpcao () {
    PetShop **petShops = NULL;
    petShops = new PetShop*[3];

    petShops[0] = new MeuCaninoFeliz("Meu Canino Feliz", 20, 40, 2);
    petShops[1] = new VaiRex("Vai Rex", 15, 50, 1.7);
    petShops[2] = new ChowChawgas("Chow Chawgas", 30, 45, 0.8);

    int melhorOpcao = -1;
    float preco = -1;

    decidirMelhorOpcao(petShops, "11/03/2018", 3, 1, 1, preco, melhorOpcao);

    assert(preco == 72);
    assert(melhorOpcao == 0);
    
    for(int i = 0; i < 3; i++) {
        delete petShops[i];
    }
    delete petShops;
}