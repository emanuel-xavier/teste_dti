#ifndef PETSHOP_HPP
#define PETSHOP_HPP

#include <string>

using namespace std;

class PetShop {
    protected:
        string nome;
        float pequeno;
        float grande;
        float distancia;
    public:
        PetShop (string, float, float, float);
        PetShop();
        virtual ~PetShop () = 0;
        virtual void fimDeSemana (float&, float&);
        string getNome ();
        float getPequeno (bool);
        float getGrande (bool);
        float getDistancia ();
        void setNome (string);
        void setPequeno (float);
        void setGrande (float);
        void setDistancia (float);
        float calcularPreco (bool, int, int);
};

#endif