#include "petshop.hpp"
#include <iostream>

PetShop::PetShop (string nome, float pequeno, float grande, float distancia) {
    this->nome = nome;
    this->pequeno = pequeno;
    this->grande = grande;
    this->distancia = distancia;
}

PetShop::PetShop () {
    this->nome = "";
    this->pequeno = 0;
    this->grande = 0;
    this->distancia = 0;
}

PetShop::~PetShop() {
    
}

void PetShop::fimDeSemana (float &pequeno, float &grande) {
    pequeno = this->pequeno;
    grande = this->grande;
}

string PetShop::getNome() {
    return this->nome;
}

float PetShop::getPequeno (bool finalDeSemana) {
    if(finalDeSemana) {
        float p = 0, g = 0;
        this->fimDeSemana(p, g);

        return p;
    }
    return this->pequeno;
}

float PetShop::getGrande (bool finalDeSemana) {
    if(finalDeSemana) {
        float p = 0, g = 0;
        this->fimDeSemana(p, g);

        return g;
    }
    return this->grande;
}

float PetShop::getDistancia () {
    return this->distancia;
}

void PetShop::setPequeno (float pequeno) {
    this->pequeno = pequeno;
}

void PetShop::setGrande (float grande) {
    this->grande = grande;
}

void PetShop::setDistancia (float distancia) {
    this->distancia = distancia;
}

void PetShop::setNome (string nome) {
    this->nome = nome;
}

float PetShop::calcularPreco (bool fimDeSemana, int nGrandes, int nPequenos) {
    float total = 0;
    if(fimDeSemana) {
        float pPequeno = this->getPequeno(fimDeSemana), pGrande = this->getGrande(fimDeSemana);

        total += nGrandes * pGrande;
        total += nPequenos * pPequeno;

        return total;
    }

    total += nGrandes * grande;
    total += nPequenos * pequeno;

    return total;
}