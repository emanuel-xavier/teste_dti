#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>
#include <stdlib.h>

#include "../models/petshop.hpp"

using namespace std;

class Util {
    public:
        float preco;
        float distancia;
        int posicao;
        Util(float, float, int);
        Util();
        ~Util();
        bool operator> (Util);
        bool operator< (Util);
        void operator= (Util);
};

bool EhFinalDeSemana (string);
void quickSort(Util[ ], int, int);
void decidirMelhorOpcao (PetShop**, string, int, int, int, float&, int&);

#endif