#include <iostream>
#include <list>

#include "./models/petshop.hpp"
#include "./utils/utils.hpp"


using namespace std;

class MeuCaninoFeliz : public PetShop {
    public:
        MeuCaninoFeliz(string nome, float pequeno, float grande, float distancia) : PetShop(nome, pequeno, grande, distancia) {
        }

        ~MeuCaninoFeliz () {

        }

        void fimDeSemana (float &pequeno, float &grande) {
            pequeno = this->pequeno * 1.2;
            grande = this->grande * 1.2;
        }
};

class VaiRex : public PetShop {
    public:
        VaiRex(string nome, float pequeno, float grande, float distancia) : PetShop(nome, pequeno, grande, distancia) {
        }

        ~VaiRex () {

        }

        void fimDeSemana (float &pequeno, float &grande) {
            pequeno = this->pequeno + 5;
            grande = this->grande + 5;
        }
};

class ChowChawgas : public PetShop {
    public:
        ~ChowChawgas() {

        }

        ChowChawgas(string nome, float pequeno, float grande, float distancia) : PetShop(nome, pequeno, grande, distancia) {
        }
};


int main(int argc, char *argv[ ]){
    if(argc != 4){
        cout << "ERROR!: Formato de entrada incorreto. Tente <dd/mm/aaaa> <quantidade de caes pequenos> <quantidade caes grandes>" << endl;
        return 0;
    }

    //012345678910
    //dd/mm/aaaa\0

    if(argv[1][10] != '\0') {
        cout << "ERROR!: Formato de data invalido. Tente <dd/mm/aaaa>" << endl;
        return 0;
    }

    for(int i = 0; i < 10; i++) {
        if(i == 2 || i == 5) {
            if(argv[1][i] != '/'){
                cout << "ERROR!: Formato de data invalido, apenas numeros e '/'. Tente <dd/mm/aaaa>" << endl;
                return 0;
            }
        }
        else {
            if(argv[1][i] < 48 || argv[1][i] > 57){
                cout << "ERROR!: Formato de data invalido, apenas numeros e '/'. Tente <dd/mm/aaaa>" << endl;
                return 0;
            }
        }

    }

    int nPequeno = atoi(argv[2]);
    int nGrande = atoi(argv[3]);
    string data = argv[1];

    PetShop **petShops = NULL;
    petShops = new PetShop*[3];

    petShops[0] = new MeuCaninoFeliz("Meu Canino Feliz", 20, 40, 2);
    petShops[1] = new VaiRex("Vai Rex", 15, 50, 1.7);
    petShops[2] = new ChowChawgas("Chow Chawgas", 30, 45, 0.8);
    
    int melhorOpcao = 0;
    float preco = 0;

    decidirMelhorOpcao(petShops, data, 3, nGrande, nPequeno, preco, melhorOpcao);


    cout << "PetShop: " << petShops[melhorOpcao]->getNome()  << "  Preco total: R$" << preco << endl;


    for(int i = 0; i < 3; i++)
        delete petShops[i];
    delete petShops;

    return 0;
}
